# Build a datatype explorer program

HOSTNAME=$(shell hostname --short)
$(info $(HOSTNAME))

CC     = gcc
CFLAGS = -g -Wall -DHOST=\"$(HOSTNAME)\"

TARGET = datatypes

all: $(TARGET)

# For now, do not use Make's % expansion parameters.  Each file must have its own 
# build instructions.  We will use the % expansion later.

datatypes.o: datatypes.c datatypes.h char.h short.h
	$(CC) $(CFLAGS) -c datatypes.c

char.o: char.c char.h
	$(CC) $(CFLAGS) -c char.c

short.o: short.c short.h
	$(CC) $(CFLAGS) -c short.c

datatypes: datatypes.o char.o short.o
	$(CC) $(CFLAGS) -o $(TARGET) datatypes.o char.o short.o

clean:
	rm -f *.o $(TARGET)

